package br.up.edu.exemenudeactivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void abrirExe01(View v){

        Intent intent = new Intent(this, Exe1Activity.class);
        startActivity(intent);

    }
    public void abrirExe2(View v){

        Intent intent = new Intent(this, Exe2Activity.class);
        startActivity(intent);

    }

}
